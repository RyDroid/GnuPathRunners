# Copyright (C) 2014-2017  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# Directories and files
SRC_DIR=src
SRC_JS_DIR=$(SRC_DIR)/js
BUILD_DIR=build
DOC_DIR=doc
JAVA_DIR=$(SRC_DIR)/java
FILES_FOR_APP=css/ images/ js/ favicon.ico index.html
FILES_FOR_ARCHIVE=src/ makefile \
	licenses/ *.md \
	.gitignore .editorconfig
PACKAGE_NAME=gnu-path-runners

# Commands
RM=rm -f
CLOSURE_COMPILER_CHECK=closure-compiler \
	--warning_level VERBOSE --js_output_file /dev/null
JAVAC=javac


.PHONY: \
	default all test \
	$(DOC_DIR) list rm-builds clean all \
	all-archives dist archive-default \
	archive-zip archive-tar-gz archive-tar-bz2 archive-tar-xz archive-7z \
	all-apps website firefox-app chromium-app \
	install uninstall


default: test archive-default all-apps

all: test doc all-archives all-apps


test: #test-javascript

test-javascript: test-closure-compiler

test-closure-compiler:
	$(CLOSURE_COMPILER_CHECK) \
		$(SRC_JS_DIR)/utils/* $(SRC_JS_DIR)/specific/*


# List of source files
list:
	ls -- $(SRC_DIR)/

clean: rm-builds

# Remove the builds
rm-builds:
	$(RM) -r -- $(BUILD_DIR)/


all-archives: \
	archive-tar-gz archive-tar-bz2 archive-tar-xz \
	archive-zip archive-7z

dist: archive-default

archive-default: archive-tar-xz

# Zip archive and compression of the source code
archive-zip: $(BUILD_DIR)/$(PACKAGE_NAME).zip
$(BUILD_DIR)/$(PACKAGE_NAME).zip: $(FILES_FOR_ARCHIVE)
	@mkdir -p $(BUILD_DIR)/
	zip $(BUILD_DIR)/$(PACKAGE_NAME).zip -r -- \
		$(FILES_FOR_ARCHIVE)

# Tar archive of the source code with gz compression
archive-tar-gz: $(BUILD_DIR)/$(PACKAGE_NAME).tar.gz
$(BUILD_DIR)/$(PACKAGE_NAME).tar.gz: $(FILES_FOR_ARCHIVE)
	@mkdir -p $(BUILD_DIR)/
	tar -zcvf $(BUILD_DIR)/$(PACKAGE_NAME).tar.gz -- \
		$(FILES_FOR_ARCHIVE)

# Tar archive of the source code with bz2 compression
archive-tar-bz2: $(BUILD_DIR)/$(PACKAGE_NAME).tar.bz2
$(BUILD_DIR)/$(PACKAGE_NAME).tar.bz2: $(FILES_FOR_ARCHIVE)
	@mkdir -p $(BUILD_DIR)/
	tar -jcvf $(BUILD_DIR)/$(PACKAGE_NAME).tar.bz2 -- \
		$(FILES_FOR_ARCHIVE)

# Tar archive of the source code with xz compression
archive-tar-xz: $(BUILD_DIR)/$(PACKAGE_NAME).tar.xz
$(BUILD_DIR)/$(PACKAGE_NAME).tar.xz: $(FILES_TO_ARCHIVE)
	@mkdir -p $(BUILD_DIR)/
	tar -Jcvf $(BUILD_DIR)/$(PACKAGE_NAME).tar.xz -- \
		$(FILES_FOR_ARCHIVE)

# 7z archive of the source code
archive-7z: $(BUILD_DIR)/$(PACKAGE_NAME).7z
$(BUILD_DIR)/$(PACKAGE_NAME).7z: $(FILES_TO_ARCHIVE)
	@mkdir -p $(BUILD_DIR)/
	7z a -t7z $(BUILD_DIR)/$(PACKAGE_NAME).7z \
		$(FILES_FOR_ARCHIVE)


# Packages for all supported "platforms"
all-apps: website firefox-app chromium-app

# Files for hosting the app on the website
website: $(SRC_DIR)/
	@mkdir -p $(BUILD_DIR)/website/
	cd $(SRC_DIR)/ && \
		cp -r -- \
			$(FILES_FOR_APP) .htaccess manifest.webapp \
			../$(BUILD_DIR)/website/

# Package for Firefox/Iceweasel/IceCat and Boot 2 Gecko (non brand name of Firefox OS)
firefox-app: $(SRC_DIR)/
	@mkdir -p $(BUILD_DIR)/
	cd $(SRC_DIR)/ && \
		zip firefox-app.zip -r -- $(FILES_FOR_APP) manifest.webapp
	mv $(SRC_DIR)/firefox-app.zip $(BUILD_DIR)/

# Package for Chromium and the non-free Google Chrome
# manifest notes:
# * Chromium wants a .json, not a .webapp
# * defaut_locale is used by Chromium for his non standard i18n and a _locales/ is needed with it
chromium-app: $(SRC_DIR)/
	@mkdir -p $(BUILD_DIR)/
	grep -v default_locale $(SRC_DIR)/manifest.webapp > $(SRC_DIR)/manifest.json
	cd $(SRC_DIR)/; \
		zip chromium-app.zip -r -- $(FILES_FOR_APP) manifest.json
	rm -f $(SRC_DIR)/manifest.json
	mv $(SRC_DIR)/chromium-app.zip $(BUILD_DIR)/

# Package for Android (and the 100% free/libre fork Replicant)
#android-app:
# TODO
# see https://github.com/turtl/mobile

debian-package:
	debuild --lintian

# rpm-package
# TODO

# Package for Ubuntu
# TODO
# https://developer.ubuntu.com/publish/webapp/packaging-web-apps/

# Package for Tizen
# TODO

# Package for Windows and ReactOS
# TODO

# Package for OS X
# TODO

# Package for JVM (Java Virtual Machine)
#jar-app: $(SRC_DIR)/
#	@mkdir -p $(BUILD_DIR)/
#	cd $(JAVA_DIR) && $(JAVAC) WebDeviceInformation.java
#	cd $(JAVA_DIR) && jar -cmf manifest.mf web-device-information.jar *.class ../index.html ../html/ ../css/ ../js/ ../images/ ../audio/ ../video/
#	TOFIX Excepted *.class, packaged files are not used and the jar must be in $(JAVA_DIR)!
#	mv $(JAVA_DIR)/web-device-information.jar $(BUILD_DIR)/web-device-information.jar
#	$(RM) $(JAVA_DIR)/*.class


install:
	@mkdir -p -- $(DESTDIR)/usr/share/$(PACKAGE_NAME)
	cp -r $(SRC_DIR)/* $(DESTDIR)/usr/share/$(PACKAGE_NAME)
	@mkdir -p -- $(DESTDIR)/usr/share/applications/
	cp -- \
		install/gnu-path-runners.desktop \
		$(DESTDIR)/usr/share/applications/
	@mkdir -p -- $(DESTDIR)/usr/games/
	printf '#!/bin/sh\nx-www-browser $(DESTDIR)/usr/share/$(PACKAGE_NAME)/index.html' \
		> $(DESTDIR)/usr/games/gnu-path-runners
	chmod +x $(DESTDIR)/usr/games/gnu-path-runners

uninstall:
	$(RM) -rf -- \
		$(DESTDIR)/usr/share/$(PACKAGE_NAME) \
		$(DESTDIR)/usr/share/applications/gnu-path-runners.desktop \
		$(DESTDIR)/usr/bin/gnu-path-runners


# Documentation
$(DOC_DIR): $(SRC_JS_DIR)
	jsdoc --unique --allfunction --recurse=9 \
		--directory=$(DOC_DIR) $(SRC_JS_DIR)
