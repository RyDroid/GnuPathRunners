/*
 * Copyright (C) 2015, Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


if(typeof(Set) != 'function' || typeof(Set.prototype) != 'object')
{
	/**
	 * @constructor
	 */
	function Set(iterable)
	{
		'use strict';
		
		this.clear();
		
		if(typeof(iterable) == 'object')
		{
			if(iterable instanceof Array)
			{
				for(var index = 0; index < iterable.length; ++index)
				{
					this.add(iterable[index]);
				}
			}
			else if(typeof(iterable.next) === 'function')
			{
				while(true)
				{
					var current = iterable.next();
					if(current.done)
						break;
					this.add(current.value);
				}
			}
		}
	}
}

if(typeof(Set.prototype.add) != 'function')
{
	/**
	 * @param  {*}       value
	 * @return {boolean}
	 */
	Set.prototype.add = function(value)
	{
		'use strict';
		
		if(this.has(value))
			return false;
		
		this.values.push(value);
		++this.size;
		return true;
	};
}

if(typeof(Set.prototype.clear) != 'function')
{
	Set.prototype.clear = function()
	{
		'use strict';
		this.values = [];
		this.size = 0;
	};
}

if(typeof(Set.prototype['delete']) != 'function')
{
	/**
	 * @param  {*}       value
	 * @return {boolean}
	 */
	Set.prototype['delete'] = function(value)
	{
		'use strict';
		
		var index = this.values.indexOf(value);
		if (index < 0)
			return false;
		this.values.splice(index, 1);
		--this.size;
		return true;
	};
}

if(typeof(Set.prototype.forEach) != 'function')
{
	Set.prototype.forEach = function(callback, thisArg)
	{
		'use strict';
		this.values.forEach(callback, thisArg);
	};
}

if(typeof(Set.prototype.has) != 'function')
{
	/**
	 * @return {boolean}
	 */
	Set.prototype.has = function(value)
	{
		'use strict';
		return this.values.includes(value);
	};
}

if(typeof(Set.prototype.values) === 'function' && typeof(Set.prototype.keys) != 'function')
{
	Set.prototype.keys = Set.prototype.values;
}

if(typeof(Set.prototype.keys) === 'function' && typeof(Set.prototype.values) != 'function')
{
	Set.prototype.values = Set.prototype.keys;
}

if(typeof(Set.prototype.toArray) != 'function')
{
	/**
	 * @return {Array}
	 */
	Set.prototype.toArray = function()
	{
		if(typeof(this.values) === 'object' && this.values instanceof Array)
		{
			return this.values;
		}
		
		var iterator = null;
		if(typeof(Set.prototype.values) === 'function')
			iterator = this.values();
		if(typeof(Set.prototype.keys) === 'function')
			iterator = this.keys();
		if(iterator == null)
			return [];
		
		var array = [];
		while(true)
		{
			var current = iterator.next();
			if(current.done)
				break;
			array.push(current.value);
		}
		return array;
	};
}
