/*
 * Copyright (C) 2014, 2017  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


if (typeof(String.prototype.includes) != 'function')
{
	/**
	 * @param  {string|String} searchString
	 * @param  {number|Number} position
	 * @return {boolean} true if searchString is a part of the string object (like "LIKE %searchString%" in SQL), otherwise false
	 */
    String.prototype.includes = function(searchString, position)
    {
        return String.prototype.indexOf.apply(this, arguments) !== -1;
    };
}

if (typeof(String.prototype.trim) != 'function')
{
	/**
	 * Removes whitespace from both ends of the string
	 * @return {string|String} string trimmed
	 */
	String.prototype.trim = function()
	{
		return this.replace(/^\s+|\s+$/g, '');
	};
}

/**
 * Returns true if a variable is a string and false otherwise
 * @param  {*} variable A variable to evaluate
 * @return {boolean} True if the variable is a string and false otherwise
 */
function isString(variable)
{
	"use strict";
	return typeof(variable) == 'string' || variable instanceof String;
}
