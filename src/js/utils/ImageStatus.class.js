/*
 * Copyright (C) 2015, 2017 Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @constructor
 * @param {string|String} img_path An image path
 */
function ImageStatus(img_path)
{
	"use strict";
	
	this.isReady = false;
	this.image = new Image();
	if(img_path != null)
	{
		this.image.addEventListener(
			'load', ImageStatus.prototype.onLoad.bind(this), false
		);
		this.image.src = img_path;
	}
}

/**
 * Actions to do on load of the image
 */
ImageStatus.prototype.onLoad = function()
{
	"use strict";
	this.isReady = true;
};

/**
 * @return {Image}
 */
ImageStatus.prototype.getImage = function()
{
	"use strict";
	return this.image;
};
