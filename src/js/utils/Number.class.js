/*
 * Copyright (C) 2015, Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/*
 * It is a polyfill for Number class.
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number
 */


if(typeof(Number.isNaN) != 'function')
{
	/**
	 * @return {boolean}
	 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/isNaN
	 */
	Number.isNaN = function(value)
	{
		'use strict';
		return typeof value === 'number' && value !== value;
	};
}

if(typeof(Number.isFinite) != 'function')
{
	/**
	 * @return {boolean}
	 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/isFinite
	 */
	Number.isFinite = function(value)
	{
		'use strict';
		return typeof value === 'number' && isFinite(value);
	};
}

if(typeof(Number.isInteger) != 'function')
{
	/**
	 * @return {boolean}
	 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/isInteger
	 */
	Number.isInteger = function(value)
	{
		'use strict';
		return (
			typeof(value) === 'number' && 
			isFinite(value) && 
			Math.floor(value) === value
		);
	};
}

if(typeof(Number.isUnsignedInteger) != 'function')
{
	/**
	 * @return {boolean}
	 */
	Number.isUnsignedInteger = function(value)
	{
		'use strict';
		return Number.isInteger(value) && value >= 0;
	};
}

if(typeof(Number.parseInt) != 'function' && typeof(parseInt) == 'function')
{
	/**
	 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/parseInt
	 */
	Number.parseInt = parseInt;
}

if(typeof(Number.parseFloat) != 'function' && typeof(parseFloat) == 'function')
{
	/**
	 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/parseFloat
	 */
	Number.parseFloat = parseFloat;
}
