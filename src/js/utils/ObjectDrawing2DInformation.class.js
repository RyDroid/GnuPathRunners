/*
 * Copyright (C) 2015, Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @constructor
 * @param {Position2D} position
 * @param {number|Number} width
 * @param {number|Number} height
 */
function ObjectDrawing2DInformation(position, width, height)
{
	if(!(position instanceof Position2D))
		throw new TypeError("position must be an instance of Position2D");
	this.position = position;
	
	this.setWidth(width);
	this.setHeight(height);
}

/**
 * @param {number|Number} width
 */
ObjectDrawing2DInformation.prototype.setWidth = function(width)
{
	if(isNaN(width))
		throw new TypeError('width is not a number');
	if(width < 0)
		throw new RangeError('width must be a positive number');
	this.width = width;
};

/**
 * @param {number|Number} height
 */
ObjectDrawing2DInformation.prototype.setHeight = function(height)
{
	if(isNaN(height))
		throw new TypeError('height is not a number');
	if(height < 0)
		throw new RangeError('height must be a positive number');
	this.height = height;
};

/**
 * @return {ObjectDrawing2DInformation}
 */
ObjectDrawing2DInformation.prototype.getCopy = function()
{
	"use strict";
	return new ObjectDrawing2DInformation(this.position.getCopy(), this.width, this.height);
};

/**
 * @return {boolean}
 */
ObjectDrawing2DInformation.prototype.isInCollisionWith = function(drawingInformation)
{
	"use strict";
	return !(
		this.position.y + this.height < drawingInformation.position.y ||
		this.position.y > drawingInformation.position.y + drawingInformation.height ||
		this.position.x > drawingInformation.position.x + drawingInformation.width ||
		this.position.x + this.width < drawingInformation.position.x
	);
};
