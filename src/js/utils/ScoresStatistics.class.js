/*
 * Copyright (C) 2015, 2017  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @constructor
 */
function ScoresStatistics()
{
	"use strict";
	this.clear();
}

ScoresStatistics.prototype.clear = function()
{
	"use strict";
	this.values = [];
	this.bestScore = null;
};

/**
 * @return {Number}
 */
ScoresStatistics.prototype.size = function()
{
	"use strict";
	return this.values.length;
};

/**
 * @param {FinalScore} score A score
 */
ScoresStatistics.prototype.push = function(score)
{
	"use strict";
	
	if(!(score instanceof FinalScore))
		throw new TypeError("score must be an instance of FinalScore");
	
	this.values.push(score);
	if(this.bestScore == null || score.value > this.bestScore.value)
		this.bestScore = score;
};

/**
 * @return {Array.<FinalScore>}
 */
ScoresStatistics.prototype.toArray = function()
{
	"use strict";
	return this.values;
};

/**
 * @return {FinalScore} the best score
 */
ScoresStatistics.prototype.getBest = function()
{
	"use strict";
	return this.bestScore;
}

/**
 * @return {FinalScore} the last pushed score
 */
ScoresStatistics.prototype.getLast = function()
{
	"use strict";
	
	if(this.values.length == 0)
		return null;
	return this.values[this.values.length -1];
};

/**
 * @return {boolean}
 */
ScoresStatistics.prototype.isLastInsertedTheBest = function()
{
	"use strict";
	
	if(this.values.length == 0)
		return false;
	return this.bestScore == this.getLast();
};

/**
 * @return {number|Number}
 */
ScoresStatistics.prototype.getSumValue = function()
{
	"use strict";
	
	var resultValue = 0;
	for(var index=0; index < this.values.length; ++index)
	{
		var value = this.values[index].value;
		resultValue += value;
	}
	return resultValue;
};

/**
 * @return {number|Number}
 */
ScoresStatistics.prototype.getAverageValue = function()
{
	"use strict";
	
	if(this.values.length == 0)
		return 0;
	return this.getSumValue() / this.values.length;
};

/**
 * @return {number|Number}
 */
ScoresStatistics.prototype.getMedianValue = function()
{
	"use strict";
	
	if(this.values.length == 0)
		return 0;
	
	this.sortByValue();
	if(this.values.length % 2 == 0)
	{
		return (this.values[(this.values.length  / 2) -1].value + this.values[this.values.length / 2].value) / 2;
	}
	return this.values[(this.values.length -1) / 2].value;
};

/**
 * @param {boolean=} lastIsFirst
 */
ScoresStatistics.prototype.sortByDate = function(lastIsFirst)
{
	"use strict";
	
	if(typeof(lastIsFirst) == 'undefined')
		lastIsFirst = true;
	else if(typeof(lastIsFirst) != 'boolean')
		throw new TypeError("parameter must be undefined or a boolean");
	
	if(lastIsFirst)
	{
		this.values.sort(function(a, b)
		{
			return a.date < b.date;
		});
	}
	else
	{
		this.values.sort(function(a, b)
		{
			return a.date > b.date;
		});
	}
};

/**
 * @param {boolean=} highestIsFirst
 */
ScoresStatistics.prototype.sortByValue = function(highestIsFirst)
{
	"use strict";
	
	if(typeof(highestIsFirst) == 'undefined')
		highestIsFirst = true;
	else if(typeof(highestIsFirst) != 'boolean')
		throw new TypeError("parameter must be undefined or a boolean");
	
	if(highestIsFirst)
	{
		this.values.sort(function(a, b)
		{
			return a.value < b.value;
		});
	}
	else
	{
		this.values.sort(function(a, b)
		{
			return a.value > b.value;
		});
	}
};
