/*
 * Copyright (C) 2015, 2017  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @constructor
 * @param {number|Number} value
 * @param {function()}    onScoreChangeFunction
 */
function CurrentNumericScore(value, onScoreChangeFunction)
{
	"use strict";
	
	if(typeof(value) == 'undefined')
		this.value = 0;
	else if(isNaN(value))
		throw new TypeError('value must be a number or undefined');
	else
		this.value = value;
	
	if(typeof(onScoreChangeFunction) == 'function')
		this.onScoreChangeFunction = onScoreChangeFunction;
	else
		this.onScoreChangeFunction = null;
};

/**
 * @return {number|Number} score value
 */
CurrentNumericScore.prototype.getValue = function()
{
	"use strict";
	return this.value;
};

/**
 * @param {number|Number} newValue The new score value
 */
CurrentNumericScore.prototype.set = function(newValue)
{
	"use strict";
	
	if(typeof(newValue) == 'undefined')
		throw new TypeError('newValue must be defined');
	if(isNaN(newValue))
		throw new TypeError('newValue must be a number');
	
	if(this.value != newValue)
	{
		this.value = newValue;
		if(typeof(this.onScoreChangeFunction) == 'function')
			this.onScoreChangeFunction();
	}
};

/**
 * @param {number|Number} valueToAdd A value to add to the current score
 */
CurrentNumericScore.prototype.add = function(valueToAdd)
{
	"use strict";
	
	if(typeof(valueToAdd) == 'undefined')
		throw new TypeError('valueToAdd must be defined');
	if(isNaN(valueToAdd))
		throw new TypeError('valueToAdd must be a number');
	
	if(valueToAdd != 0)
	{
		this.value += valueToAdd;
		if(typeof(this.onScoreChangeFunction) == 'function')
			this.onScoreChangeFunction();
	}
};

/**
 * @return {FinalScore}
 */
CurrentNumericScore.prototype.toFinalScore = function()
{
	"use strict";
	return new FinalScore(this.value);
};
