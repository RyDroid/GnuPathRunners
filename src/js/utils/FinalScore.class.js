/*
 * Copyright (C) 2015, 2017  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @constructor
 * @param {number|Number} value A value
 * @param {Date=}         date  A potential date
 */
function FinalScore(value, date)
{
	"use strict";
	
	this.value = value;
	
	if(typeof(date) == 'undefined')
		this.date = new Date();
	else
		this.date = date;
}

/**
 * @return {number|Number}
 */
FinalScore.prototype.getValue = function()
{
	"use strict";
	return this.value;
};

/**
 * @return {Date}
 */
FinalScore.prototype.getDate = function()
{
	"use strict";
	return this.date;
};
