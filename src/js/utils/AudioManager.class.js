/*
 * Copyright (C) 2014-2015, Nicola Spanti (also known as RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @constructor
 * @param {Audio|HTMLAudioElement=} element  An audio tag
 * @param {boolean=}                controls show controls
 * @param {boolean=}                muted    no sound
 * @param {number|Number=}          volume   A volume between 0 and 1 (both included)
 */
function AudioManager(element, controls, muted, volume)
{
	'use strict';
	
	if(element == undefined)
		this.element = document.querySelector('audio');
	else
		this.element = element;
	
	this.format = '';
	
	if(controls != undefined)
		this.element.controls = controls;
	
	if(muted == undefined)
		this.element.muted = 1;
	else
		this.element.muted = muted;
	
	if(volume == undefined)
		this.element.volume = 1;
	else
		this.element.volume = volume;
	
	if (this.element.canPlayType)
	{
		for (var mime_type in AudioManager.MIME_TYPES_WITH_MATCHING_EXTENSION)
		{
			if (this.element.canPlayType('audio/'+ mime_type))
			{
				this.format = AudioManager.MIME_TYPES_WITH_MATCHING_EXTENSION[mime_type];
				this.element.setAttribute('type', 'audio/'+ mime_type);
				break;
			}
		}
		
		if(this.format == '')
			alert('Votre navigateur web ne supporte pas un format audio compatible avec le jeu');
	}
	else
		alert('Impossible de vérifier le type de format audio supporté par votre navigateur web');
}

AudioManager.MIME_TYPES_WITH_MATCHING_EXTENSION = {
//	'flac'                  : 'flac',
//	'oga;  codecs="flac"'   : 'flac.oga',
//	'ogg;  codecs="flac"'   : 'flac.oga',
	'oga;  codecs="vorbis"' : 'vorbis.oga',
	'ogg;  codecs="vorbis"' : 'vorbis.oga',
	'oga;  codecs="opus"'   : 'opus.oga',
	'ogg;  codecs="opus"'   : 'opus.oga',
	'webm; codecs="vorbis"' : 'vorbis.webma',
	'webm; codecs="opus"'   : 'opus.webma',
	'wav;  codecs="PCM"'    : 'pcm.wav',
	'wave; codecs="PCM"'    : 'pcm.wav',
	'mp3'                   : 'mp3',
	'mpeg'                  : 'mp3'
};
// Commented lines are for unsupported formats

/**
 * {boolean}
 */
AudioManager.prototype.getMuted = function()
{
	'use strict';
	return this.element.muted;
};

/**
 * @param {boolean} muted no sound
 */
AudioManager.prototype.setMuted = function(muted)
{
	'use strict';
	this.element.muted = muted;
};

AudioManager.prototype.inverseMuted = function()
{
	'use strict';
	this.element.muted = !this.element.muted;
};

/**
 * @param {number|Number} volume A volume between 0 and 1 (both included)
 */
AudioManager.prototype.setVolume = function(volume)
{
	'use strict';
	
	if (typeof(volume) != 'number')
		throw new TypeError('volume must be a number');
	
	volume = parseFloat(volume);
	if(volume < 0)
		throw new TypeError('volume must be positive');
	if(volume > 1)
		throw new TypeError('volume must not be greater than 1');
	
	this.element.volume = volume;
};

/**
 * @param {string|String} file file name
 * @param {boolean}       loop play in loop
 */
AudioManager.prototype.play = function(file, loop)
{
	'use strict';
	
	if(loop ==  undefined)
		loop = false;
	
	if(this.format != '')
	{
		this.element.autoplay = true;
		this.element.loop = loop;
		this.element.setAttribute('src', 'audio/'+ file +'.'+ this.format);
	}
};
