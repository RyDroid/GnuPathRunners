/*
 * Copyright (C) 2014, Nicola Spanti (also known as RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


(function()
{
	/**
	 * Decimal adjustment of a number.
	 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/round
	 * @param  {string|String} type     The type of adjustment.
	 * @param  {number|Number} value    The number.
	 * @param  {number|Number} exponent The exponent (the 10 logarithm of the adjustment base).
	 * @return {number|Number}          The adjusted value.
	 */
	function decimalAdjust(type, value, exponent)
	{
		// If the exp is undefined or zero...
		if (typeof(exponent) === 'undefined' || +exponent === 0)
			return Math[type](value);
		
		value = +value;
		exponent = +exponent;
		// If the value is not a number or the exp is not an integer...
		if (isNaN(value) || !(typeof(exponent) === 'number' && exponent % 1 === 0))
			return NaN;
		
		/** @type Array.<string> */
		var values;
		// Shift
		values = value.toString().split('e');
		value = Math[type](+(values[0] + 'e' + (values[1] ? (+values[1] - exponent) : -exponent)));
		// Shift back
		values = value.toString().split('e');
		return +(values[0] + 'e' + (values[1] ? (+values[1] + exponent) : exponent));
	}
	
	// Decimal round
	if (!Math.round10)
	{
		/**
		 * @param  {number|Number} value    The number
		 * @param  {number|Number} exponent The exponent
		 */
		Math.round10 = function(value, exponent)
		{
			'use strict';
			return decimalAdjust('round', value, exponent);
		};
	}
	
	// Decimal floor
	if (!Math.floor10)
	{
		/**
		 * @param  {number|Number} value    The number
		 * @param  {number|Number} exponent The exponent
		 */
		Math.floor10 = function(value, exponent)
		{
			'use strict';
			return decimalAdjust('floor', value, exponent);
		};
	}
	
	// Decimal ceil
	if (!Math.ceil10)
	{
		/**
		 * @param  {number|Number} value    The number
		 * @param  {number|Number} exponent The exponent
		 */
		Math.ceil10 = function(value, exponent)
		{
			'use strict';
			return decimalAdjust('ceil', value, exponent);
		};
	}
})();
