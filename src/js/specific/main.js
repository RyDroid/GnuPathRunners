/*
 * Copyright (C) 2015, 2017  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/* General settings and functions */

if(typeof(screen.lockOrientation) == 'function')
{
	screen.lockOrientation("portrait-primary");
}

var menu = new GameSimpleMainMenu([
	'play',
	'lose',
	'how-to-play',
	'privacy',
	'license',
	'contribute',
	'gnu-project',
	'scores'
]);

function setMenuSectionStyle(menu)
{
	'use strict';
	
	var menuSection = menu.getElement();
	var buttons = menuSection.querySelectorAll("button");
	var buttonHeight = parseInt(0.2 * window.innerHeight, 10);
	var buttonImages = null;
	for(var i=0; i < buttons.length; ++i)
	{
		buttons[i].style.minHeight = buttonHeight +'px';
		buttons[i].style.fontSize = parseInt(buttonHeight * 0.5, 10) +'px';
		
		buttonImages = buttons[i].querySelectorAll('button img[src]');
		for(var j=0; j < buttonImages.length; ++j)
		{
			buttonImages[j].style.maxHeight = buttons[i].style.minHeight;
		}
	}
	menu.show();
}
setMenuSectionStyle(menu);

function setLoseSectionStyle()
{
	var loseSection = menu.getActivity('lose');
	var index;
	
	var buttons = loseSection.querySelectorAll('button');
	var buttonHeight    = parseInt(0.2 * window.innerHeight, 10);
	var buttonMinWidth  = parseInt(0.2 * window.innerWidth,  10);
	for(index = 0; index < buttons.length; ++index)
	{
		var button = buttons[index];
		button.style.minHeight = buttonHeight   +'px';
		button.style.minWidth  = buttonMinWidth +'px';
	}
	
	var buttonImages = loseSection.querySelectorAll('button img[src]');
	if(buttonImages != null && buttonImages.length > 0)
	{
		var buttonFontSize = parseInt(window.getComputedStyle(buttons[0], null).getPropertyValue('font-size'), 10);
		var buttonImageRatio;
		var buttonImageHeight;
		for(index = 0; index < buttonImages.length; ++index)
		{
			var buttonImage = buttonImages[index];
			buttonImageHeight = buttonHeight - buttonFontSize;
			buttonImage.style.height = buttonImageHeight + 'px';
			
			buttonImageRatio = (
				parseInt(window.getComputedStyle(buttonImage, null).getPropertyValue('width'),  10) /
				parseInt(window.getComputedStyle(buttonImage, null).getPropertyValue('height'), 10)
			);
			buttonImage.style.width  = parseInt(buttonImageHeight * buttonImageRatio, 10) + 'px';
		}
	}
	
	menu.showActivity('lose');
}

var scores = new ScoresStatistics();

function startPlaying()
{
	var game = new GnuRunnersGame(afterLosingFunction);
	
	var loseInfoElement = document.getElementById('lose-infos');
	if(loseInfoElement)
	{
		loseInfoElement.innerHTML = '';
	}
	
	menu.hide();
	menu.hideActivity('lose');
	game.canvas.style.display = 'inline';
	game.setWindowSizeOnly();
	window.addEventListener('resize', game.setWindowSize.bind(game), false);
	
	game.start();
}

function afterLosingFunction(game)
{
	window.removeEventListener('resize', game.setWindowSize.bind(game), false);
	
	scores.push(new FinalScore(game.getScoreValue()));
	if(scores.size() > 1 && scores.isLastInsertedTheBest())
	{
		var loseInfoElement = document.getElementById('lose-infos');
		if(loseInfoElement)
		{
			loseInfoElement.innerHTML += '<li>New high score</li>';
		}
	}
	menu.showButton('scores');
	
	var loseSection = menu.getActivity('lose');
	(function()
	{
		var lastScoreValueElement = loseSection.querySelector('.last-score-value');
		if(lastScoreValueElement)
		{
			lastScoreValueElement.innerHTML = new String(scores.getLast().value);
		}
	})();
	(function()
	{
		var bestScoreValueElement = loseSection.querySelector('.best-score-value');
		if(bestScoreValueElement)
		{
			bestScoreValueElement.innerHTML = new String(scores.getBest().value);
		}
	})();
	
	setLoseSectionStyle();
}

function scoresSectionToMenu()
{
	menu.onClickGoToMenu("scores");
	
	(function()
	{
		var scoresTableBodyElement = document.querySelector('#scores tbody');
		if(scoresTableBodyElement)
		{
			scoresTableBodyElement.innerHTML = '';
		}
	})();
	
	(function()
	{
		var scoresStatsListElement = document.querySelector('#scores_statistics + ul');
		if(scoresStatsListElement)
		{
			scoresStatsListElement.innerHTML = '';
		}
	})();
}

/**
 * @return boolean
 */
function confirmRemoveScoresHistory()
{
	"use strict";
	return window.confirm("This will remove your entire history of scores.");
}

/**
 * @return boolean
 */
function confirmRemoveScoresHistoryAndDo()
{
	"use strict";
	if(confirmRemoveScoresHistory())
	{
		menu.hideButton('scores');
		scoresSectionToMenu();
		scores.clear();
		return true;
	}
	return false;
}


/* Events */
(function()
{
	document.getElementById("button_play").addEventListener ('click', startPlaying, false);
	document.getElementById("button_retry").addEventListener('click', startPlaying, false);
	
	menu.addDefaultEventListenersToButton('how-to-play');
	
	document.getElementById("button_how-to-play_ok").addEventListener('click', function()
	{
		menu.onClickGoToMenu("how-to-play");
	}, false);
	
	document.getElementById("button_lose_go-menu").addEventListener('click', function()
	{
		menu.show();
		menu.hideActivity('lose');
		menu.hideActivity('play');
	}, false);
	
	menu.addDefaultEventListenersToButton('privacy');
	
	document.getElementById("button_privacy_ok").addEventListener('click', function()
	{
		menu.onClickGoToMenu('privacy');
	}, false);
	
	menu.addDefaultEventListenersToButton('license');
	
	document.getElementById("button_license_ok").addEventListener('click', function()
	{
		menu.onClickGoToMenu('license');
	}, false);
	
	menu.addDefaultEventListenersToButton('contribute');
	
	document.getElementById("button_contribute_ok").addEventListener('click', function()
	{
		menu.onClickGoToMenu('contribute');
	}, false);
	
	menu.addDefaultEventListenersToButton('gnu-project');
	
	document.getElementById("button_gnu-project_ok").addEventListener('click', function()
	{
		menu.onClickGoToMenu('gnu-project');
	}, false);
	
	document.getElementById("button_scores").addEventListener('click', function()
	{
		var scoresSection = menu.getActivity('scores');
		
		(function()
		{
			var lastScoreValue = scoresSection.querySelector('.last-score-value');
			if(lastScoreValue)
			{
				lastScoreValue.innerHTML = new String(scores.getLast().value);
			}
		})();
		(function()
		{
			var bestScoreValue = scoresSection.querySelector('.best-score-value');
			if(bestScoreValue)
			{
				bestScoreValue.innerHTML = new String(scores.getBest().value);
			}
		})();
		
		menu.onClickGoToActivity('scores');
		
		(function()
		{
			var tableOfScoresBody = scoresSection.querySelector('tbody');
			scores.sortByDate();
			var scoresArray = scores.toArray();
			for(var index = 0; index < scoresArray.length; ++index)
			{
				var score = scoresArray[index];
				tableOfScoresBody.innerHTML += '<tr><td>'+ new String(score.value) +'</td><td>'+ score.date.toLocaleString() +'</td></tr>';
			}
		})();
		
		(function()
		{
			var addItemToStatsList = (function()
			{
				var statsList = document.querySelector('#scores_statistics + ul');
				if(!statsList)
				{
					throw new Error('The list element for statistics was not found!');
				}
				
				return function(htmlOfItem)
				{
					if(!isString(htmlOfItem))
						throw new TypeError('htmlOfItem must be a string');
					statsList.innerHTML += '<li>'+ htmlOfItem +'</li>';
				}
			})();
			
			addItemToStatsList(
				'Number of scores = '
				+ new String(scores.size())
			);
			addItemToStatsList(
				'Sum of scores = '
				+ new String(scores.getSumValue())
			);
			addItemToStatsList(
				'Average score = '
				+ new String(Math.round10(scores.getAverageValue(), -2))
			);
			addItemToStatsList(
				'Median score = '
				+ new String(Math.round10(scores.getMedianValue(),  -2))
			);
		})();
	}, false);
	
	(function()
	{
		var buttonScoresOk = document.getElementById("button_scores_ok");
		if(buttonScoresOk)
		{
			buttonScoresOk.addEventListener(
				'click', scoresSectionToMenu, false
			);
		}
	})();
	
	(function()
	{
		var buttonScoresClear = document.getElementById("button_scores_clear");
		if(buttonScoresClear)
		{
			buttonScoresClear.addEventListener(
				'click', confirmRemoveScoresHistoryAndDo, false
			);
		}
	})();
})();
