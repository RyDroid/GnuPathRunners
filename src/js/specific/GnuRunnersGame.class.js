/*
 * Copyright (C) 2015, 2017, Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @constructor
 */
function GnuRunnersGame(afterLosingFunction)
{
	"use strict";
	
	this.canvas = document.querySelector('canvas');
	if(this.canvas == null)
	{
		throw new Error('There is no canvas');
	}
	
	if(typeof(this.canvas.getContext) != 'function')
	{
		throw new Error('The canvas can not get a context');
	}
	this.context = this.canvas.getContext('2d');
	if(this.context == null)
	{
		throw new Error('There is no context of the canvas');
	}
	
	this.audio = new AudioManager(
		document.querySelector('audio'), false, false
	);
	
	this.paths = new Array(2);
	for(var index=0; index < this.paths.length; ++index)
	{
		this.paths[index] = new NearRunnerPathsGroup(2);
		this.paths[index].paths[0].runner = new GnuRunner();
	}
	
	this.clock = new Clock();
	this.timeInMillisecondsBetweenEachFrame = 40;
	this.frameDeltaTimeFactor = 1;
	this.idDrawingForAFrame = null;
	this.distancePerMicroseconds = 0.08;
	
	this.afterLosingFunction = afterLosingFunction;
}

GnuRunnersGame.prototype.setWindowSizeOnly = function()
{
	"use strict";
	if(this.canvas == null)
	{
		return false;
	}
	this.canvas.height = window.innerHeight;
	this.canvas.width = this.canvas.height * 720/1280;
	return true;
};

GnuRunnersGame.prototype.setElementsDrawingInformation = function()
{
	"use strict";
	
	var width  = this.canvas.width / this.paths.length;
	var height = this.canvas.height;
	for(var index = 0; index < this.paths.length; ++index)
	{
		var position = new Position2D(index / this.paths.length * this.canvas.width, 0);
		var path = this.paths[index];
		path.setDrawingInformation(
			new ObjectDrawing2DInformation(position, width, height)
		);
	}
};

GnuRunnersGame.prototype.setWindowSize = function()
{
	"use strict";
	this.setWindowSizeOnly();
	this.setElementsDrawingInformation();
};

/**
 * @return {number|Number}
 */
GnuRunnersGame.prototype.getPercentageReady = function()
{
	"use strict";
	
	if(this.paths == null)
	{
		return 0;
	}
	
	var percentage = 0;
	for(var index = 0; index < this.paths.length; ++index)
	{
		var path = this.paths[index];
		percentage += path.getPercentageReady();
	}
	return percentage / this.paths.length;
};

/**
 * @return {boolean}
 */
GnuRunnersGame.prototype.isReady = function()
{
	"use strict";
	
	if(this.paths == null)
	{
		return false;
	}
	
	for(var index=0; index < this.paths.length; ++index)
	{
		var path = this.paths[index];
		if(!path.isReady())
		{
			return false;
		}
	}
	return true;
};

/**
 * @return {boolean}
 */
GnuRunnersGame.prototype.isOver = function()
{
	"use strict";
	
	for(var i=0, j; i < this.paths.length; ++i)
	{
		for(j=0; j < this.paths[i].paths.length; ++j)
		{
			if(
				this.paths[i].paths[j].runner instanceof GnuRunner &&
				!this.paths[i].paths[j].runner.isAlive
			)
			{
				return true;
			}
		}
	}
	return false;
};

GnuRunnersGame.prototype.eventKeyUpListener = function(event)
{
	"use strict";
	
	if(this.paths.length > 0 && !this.isOver())
	{
		if(event.keyCode == KeyEvent.DOM_VK_0)
		{	
			this.paths[0].changeRunnerToAnOtherPath();
		}
		else if(event.keyCode == KeyEvent.DOM_VK_1)
		{
			if(this.paths.length > 1)
				this.paths[1].changeRunnerToAnOtherPath();
		}
		else if(event.keyCode == KeyEvent.DOM_VK_2)
		{
			if(this.paths.length > 2)
				this.paths[2].changeRunnerToAnOtherPath();
		}
		else if(event.keyCode == KeyEvent.DOM_VK_3)
		{
			if(this.paths.length > 3)
				this.paths[3].changeRunnerToAnOtherPath();
		}
		else if(event.keyCode == KeyEvent.DOM_VK_4)
		{
			if(this.paths.length > 4)
				this.paths[4].changeRunnerToAnOtherPath();
		}
		else if(event.keyCode == KeyEvent.DOM_VK_5)
		{
			if(this.paths.length > 5)
				this.paths[5].changeRunnerToAnOtherPath();
		}
		else if(event.keyCode == KeyEvent.DOM_VK_6)
		{
			if(this.paths.length > 6)
				this.paths[6].changeRunnerToAnOtherPath();
		}
		else if(event.keyCode == KeyEvent.DOM_VK_7)
		{
			if(this.paths.length > 7)
				this.paths[7].changeRunnerToAnOtherPath();
		}
		else if(event.keyCode == KeyEvent.DOM_VK_8)
		{
			if(this.paths.length > 8)
				this.paths[8].changeRunnerToAnOtherPath();
		}
		else if(event.keyCode == KeyEvent.DOM_VK_9)
		{
			if(this.paths.length > 9)
				this.paths[9].changeRunnerToAnOtherPath();
		}
		else if(
			event.keyCode == KeyEvent.DOM_VK_LEFT ||
			event.keyCode == KeyEvent.DOM_VK_H /* like Vim */
			)
		{	
			this.paths[0].changeRunnerToAnOtherPath();
		}
		else if(
			event.keyCode == KeyEvent.DOM_VK_RIGHT ||
			event.keyCode == KeyEvent.DOM_VK_L /* like Vim */
			)
		{
			this.paths[this.paths.length -1].changeRunnerToAnOtherPath();
		}
		else if(event.keyCode == KeyEvent.DOM_VK_SPACE)
		{
			this.paths[parseInt(this.paths.length /2, 10)].changeRunnerToAnOtherPath();
		}
	}
};

GnuRunnersGame.prototype.getPathFromEvent = function(event)
{
	"use strict";
	var bodyWidth = document.querySelector('body').clientWidth;
	var index = parseInt(event.clientX / bodyWidth * this.paths.length, 10);
	var path = this.paths[index];
	return path;
};

GnuRunnersGame.prototype.eventClickListener = function(event)
{
	"use strict";
	
	if(this.paths.length > 0 && !this.isOver())
	{
		var path = this.getPathFromEvent(event);
		path.changeRunnerToAnOtherPath();
	}
};

GnuRunnersGame.prototype.addEventListeners = function()
{
	"use strict";
	window.addEventListener('keyup', this.eventKeyUpListener.bind(this), false);
	window.addEventListener('click', this.eventClickListener.bind(this), false);
};

GnuRunnersGame.prototype.removeEventListeners = function()
{
	"use strict";
	window.removeEventListener(
		'keyup', this.eventKeyUpListener.bind(this), false
	);
	window.removeEventListener(
		'click', this.eventClickListener.bind(this), false
	);
};

GnuRunnersGame.prototype.manageGamepadEvent = function(gamepad)
{
	"use strict";
	
	if(isGamepadWithStandardMapping(gamepad))
	{
		var leftFirstTriggerButton  = getGamepadButtonsForLeftTriggersUnsafe      (gamepad)[0];
		var rightFirstTriggerButton = getGamepadButtonsForRightTriggersUnsafe     (gamepad)[0];
		var leftDirectionalPad      = getGamepadButtonsForLeftDirectionalPadUnsafe(gamepad);
		
		if(isGamepadButtonFullyPressedUnsafe(leftFirstTriggerButton) ||
		   isGamepadButtonFullyPressedUnsafe(leftDirectionalPad.left))
		{
			this.paths[0].changeRunnerToAnOtherPath();
		}
		if(isGamepadButtonFullyPressedUnsafe(rightFirstTriggerButton) ||
		   isGamepadButtonFullyPressedUnsafe(leftDirectionalPad.right))
		{
			this.paths[this.paths.length -1].changeRunnerToAnOtherPath();
		}
	}
};

GnuRunnersGame.prototype.manageGamepadsEvents = function()
{
	"use strict";
	
	var gamepads = getGamepads();
	if(!isGamepadArrayEmpty(gamepads))
	{
		for(var index = 0; index < gamepads.length; ++index)
		{
			var gamepad = gamepads[index];
			this.manageGamepadEvent(gamepad);
		}
	}
};

GnuRunnersGame.prototype.requestAnimationFrameCallback = function()
{
	this.updateAndDraw();
	
	// Without this condition, even after cancelAnimationFrame, this function is called!
	if(this.idDrawingForAFrame != null)
	{
		this.idDrawingForAFrame = window.requestAnimationFrame(
			this.requestAnimationFrameCallback.bind(this)
		);
	}
};

GnuRunnersGame.prototype.start = function()
{
	this.setElementsDrawingInformation();
	
	function waitReady(element)
	{
		setTimeout(function() {
			/* This verification fix problem with not fully loaded images (NS_ERROR_NOT_AVAILABLE) */
			if(element.isReady())
			{
				element.addEventListeners();
				element.clock.start();
				if(
					typeof(window.requestAnimationFrame) == 'function' &&
					typeof(window.cancelAnimationFrame)  == 'function'
				)
				{
					element.idDrawingForAFrame = 0;
					element.idDrawingForAFrame = window.requestAnimationFrame(
						element.requestAnimationFrameCallback.bind(element)
					);
				}
				else
				{
					element.idDrawingForAFrame = window.setInterval(
						element.updateAndDraw.bind(element),
						element.timeInMillisecondsBetweenEachFrame
					);
				}
			}
			else
			{
				waitReady(element);
			}
		}, 10, element);
	}
	waitReady(this);
};

GnuRunnersGame.prototype.stop = function()
{
	"use strict";
	
	if(
		typeof(window.requestAnimationFrame) == 'function' &&
		typeof(window.cancelAnimationFrame)  == 'function'
	)
	{
		window.cancelAnimationFrame(this.idDrawingForAFrame);
	}
	else
	{
		window.clearInterval(this.idDrawingForAFrame);
	}
	this.idDrawingForAFrame = null;
	
	this.clock.stop();
	this.removeEventListeners();
};

GnuRunnersGame.prototype.lose = function()
{
	"use strict";
	
	this.stop();
	
	if(typeof(this.afterLosingFunction) === 'function')
		this.afterLosingFunction(this);
};

/**
 * @return {number|Number} score value
 */
GnuRunnersGame.prototype.getScoreValue = function()
{
	"use strict";
	
	var score = 0;
	for(var index = 0; index < this.paths.length; ++index)
	{
		var path = this.paths[index];
		score += path.getScore();
	}
	return score;
};

GnuRunnersGame.prototype.update = function()
{
	"use strict";
	
	if(this.isOver())
	{
		this.lose();
	}
	else
	{
		this.clock.update();
		this.frameDeltaTimeFactor = this.clock.getDeltaTime() / this.timeInMillisecondsBetweenEachFrame;
		// If the computer has enough power to make a frame each this.timeInMillisecondsBetweenEachFrame, factor=1
		// But, if for example the computer needs more time to make a frame,
		// this factor will permit to run the game as the normal speed even if there is less FPS
		
		this.manageGamepadsEvents();
		for(var index = 0; index < this.paths.length; ++index)
		{
			var path = this.paths[index];
			path.update(this.clock, this.distancePerMicroseconds);
		}
	}
};

GnuRunnersGame.prototype.updateAndDraw = function()
{
	"use strict";
	this.update();
	GnuRunnersGameDrawer2D.drawGame(this);
};
