/*
 * Copyright (C) 2015, Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @constructor
 */
function RunnerPathItem()
{
	"use strict";
	
	this.isGood = Math.random() < 0.5;
	
	if(this.isGood)
	{
		this.image = ImagesStatusCache.getImageStatus('images/items/good/bitcoin.svg');
	}
	else
	{
		this.image = ImagesStatusCache.getImageStatus('images/items/bad/defective-by-design.svg');
	}
}

/**
 * @return {Image}
 */
RunnerPathItem.prototype.getImage = function()
{
	"use strict";
	return this.image.image;
};

/**
 * @return {boolean}
 */
RunnerPathItem.prototype.isReady = function()
{
	"use strict";
	return this.image.isReady;
};

/**
 * @param {ObjectDrawing2DInformation} drawingInformation
 */
RunnerPathItem.prototype.setDrawingInformation = function(drawingInformation)
{
	"use strict";
	if(!(drawingInformation instanceof ObjectDrawing2DInformation))
	{
		throw new TypeError('drawingInformation must be an instance of ObjectDrawing2DInformation');
	}
	this.drawingInformation = drawingInformation;
};

/**
 * @param {Clock}         clock
 * @param {number|Number} distancePerMicroseconds
 */
RunnerPathItem.prototype.update = function(clock, distancePerMicroseconds)
{
	"use strict";
	this.drawingInformation.position.y +=
		clock.deltaTime * distancePerMicroseconds *
		this.drawingInformation.height / 32 /* Thanks to this, the speed is relative, otherwise it would be easier with a bigger height */;
};
