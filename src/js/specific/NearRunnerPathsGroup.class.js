/*
 * Copyright (C) 2015, 2017  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @constructor
 */
function NearRunnerPathsGroup(nb)
{
	"use strict";
	
	this.paths = [];
	if(typeof(nb) != 'undefined')
	{
		if(isNaN(nb))
		{
			throw new TypeError('nb is not a number');
		}
		else
		{
			this.paths = new Array(nb);
			for(var index=0; index < nb; ++index)
			{
				this.paths[index] = new RunnerPath();
			}
		}
	}
	
	this.distance = 0;
}

/**
 * @return {number|Number}
 */
NearRunnerPathsGroup.prototype.getPercentageReady = function()
{
	"use strict";
	
	var percentage = 0;
	for(var index=0; index < this.paths.length; ++index)
	{
		var path = this.paths[index];
		percentage += path.getPercentageReady();
	}
	return percentage / this.paths.length;
};

/**
 * @return {boolean}
 */
NearRunnerPathsGroup.prototype.isReady = function()
{
	"use strict";
	
	for(var index = 0; index < this.paths.length; ++index)
	{
		var path = this.paths[index];
		if(!path.isReady())
			return false;
	}
	return true;
};

NearRunnerPathsGroup.prototype.setDrawingInformation = function(drawingInformation)
{
	if(!(drawingInformation instanceof ObjectDrawing2DInformation))
		throw new TypeError('drawingInformation must be an instance of ObjectDrawing2DInformation');
	this.drawingInformation = drawingInformation;
	
	var pathDrawingInformationCache = this.drawingInformation.getCopy();
	pathDrawingInformationCache.setWidth(this.drawingInformation.width / this.paths.length);
	for(var index=0; index < this.paths.length; ++index)
	{
		var pathDrawingInformation = pathDrawingInformationCache.getCopy();
		pathDrawingInformation.position.setX(
			pathDrawingInformation.position.x +
			index * pathDrawingInformation.width
		);
		var path = this.paths[index];
		path.setDrawingInformation(pathDrawingInformation);
	}
};

/**
 * @return {number|Number} number of items
 */
NearRunnerPathsGroup.prototype.getNbItems = function()
{
	"use strict";
	
	var nb = 0;
	for(var index=0; index < this.paths.length; ++index)
	{
		var path = this.paths[index];
		nb += path.items.length;
	}
	return nb;
};

NearRunnerPathsGroup.prototype.update = function(clock, distancePerMicroseconds)
{
	"use strict";
	
	this.distance += clock.deltaTime * distancePerMicroseconds;
	
	if(this.paths.length > 0)
	{
		for(var index=0; index < this.paths.length; ++index)
		{
			var path = this.paths[index];
			path.update(clock, distancePerMicroseconds);
		}
		
		var lastItemYValue = this.drawingInformation.height;
		for(var index=0; index < this.paths.length; ++index)
		{
			var path = this.paths[index];
			if(path.items.length > 0 && lastItemYValue > path.items[path.items.length -1].drawingInformation.position.y)
			{
				lastItemYValue = path.items[path.items.length -1].drawingInformation.position.y;
			}
		}
		
		if(
			lastItemYValue > (2.5 * this.paths[0].getNormalItemHeight()) &&
			Math.random() < 0.06
		)
		{
			var lastItemPath = this.paths[parseInt(Math.random() * this.paths.length, 10)];
			var item = new RunnerPathItem();
			lastItemPath.setItemDrawingInformation(item);
			lastItemPath.items.push(item);
		}
	}
};

/**
 * @return {boolean}
 */
NearRunnerPathsGroup.prototype.changeRunnerToAnOtherPath = function()
{
	"use strict";
	
	var oldRunnerPathNumber = -1;
	
	for(var index = 0; index < this.paths.length; ++index)
	{
		if(this.paths[index].runner instanceof GnuRunner)
			oldRunnerPathNumber = index;
	}
	
	if(oldRunnerPathNumber < 0)
		return false;
	
	var newRunnerPathNumber = (oldRunnerPathNumber + 1) % this.paths.length;
	this.paths[newRunnerPathNumber].runner = this.paths[oldRunnerPathNumber].runner;
	this.paths[oldRunnerPathNumber].runner = null;
	this.paths[newRunnerPathNumber].setRunnerDrawingInformation();
	return true;
};

/**
 * @return {number|Number} score
 */
NearRunnerPathsGroup.prototype.getScore = function()
{
	"use strict";
	
	var score = 0;
	for(var index = 0; index < this.paths.length; ++index)
	{
		var path = this.paths[index];
		score += path.getScore();
	}
	return score;
};
