/*
 * Copyright (C) 2015, Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @constructor
 */
function GameSimpleMainMenu(namesOfElements)
{
	'use strict';
	this.namesOfElements = new Set(namesOfElements);
}

/**
 * @param {number|Number=} value
 */
GameSimpleMainMenu.scrollTop = function(value)
{
	'use strict';
	
	if(!Number.isUnsignedInteger(value))
		value = 0;
	document.body.scrollTop = document.documentElement.scrollTop = value;
};

GameSimpleMainMenu.prototype.getElement = function()
{
	'use strict';
	return document.getElementById('menu');
};

GameSimpleMainMenu.prototype.show = function()
{
	'use strict';
	return this.getElement().style.display = 'block';
};

GameSimpleMainMenu.prototype.hide = function()
{
	'use strict';
	return this.getElement().style.display = 'none';
};

GameSimpleMainMenu.prototype.getButton = function(name)
{
	'use strict';
	
	if(!this.namesOfElements.has(name))
		return null;
	return document.getElementById('button_'+ name);
};

GameSimpleMainMenu.prototype.hideButton = function(name)
{
	'use strict';
	var button = this.getButton(name);
	if(typeof(button) == 'object' && typeof(button.style) == 'object')
	{
		button.style.display = 'none';
	}
};

GameSimpleMainMenu.prototype.showButton = function(name)
{
	'use strict';
	var button = this.getButton(name);
	if(typeof(button) == 'object' && typeof(button.style) == 'object')
	{
		button.style.display = 'inline-block';
	}
};

GameSimpleMainMenu.prototype.getActivity = function(name)
{
	'use strict';
	
	if(!this.namesOfElements.has(name))
		return null;
	return document.getElementById(name);
};

GameSimpleMainMenu.prototype.showActivity = function(name)
{
	'use strict';
	
	var activity = this.getActivity(name);
	if(activity === false)
		throw new Error(name +' is not an element of the menu');
	if(typeof(activity) != 'object' || typeof(activity.style) != 'object')
		throw new Error('Activity'+ name +' was not found');
	activity.style.display = 'block';
	GameSimpleMainMenu.scrollTop();
};

GameSimpleMainMenu.prototype.hideActivity = function(name)
{
	'use strict';
	
	var activity = this.getActivity(name);
	if(activity === false)
		throw new Error(name +' is not an element of the menu');
	if(typeof(activity) != 'object' || typeof(activity.style) != 'object')
		throw new Error('Activity'+ name +' was not found');
	activity.style.display = 'none';
};

GameSimpleMainMenu.prototype.onClickGoToMenu = function(currentElementName)
{
	'use strict';
	
	if(!this.namesOfElements.has(currentElementName))
		throw new Error(currentElementName +' is not an element of the menu');
	
	this.show();
	this.hideActivity(currentElementName);
	GameSimpleMainMenu.scrollTop();
};

GameSimpleMainMenu.prototype.onClickGoToActivity = function(activityName)
{
	'use strict';
	
	if(!this.namesOfElements.has(activityName))
		throw new Error(activityName +' is not an element of the menu');
	
	this.showActivity(activityName);
	this.hide();
};

GameSimpleMainMenu.DEFAULT_BUTTON_EVENT_LISTENER_TYPES = new Set(['click']);

GameSimpleMainMenu.prototype.addDefaultEventListenerToButtonUnsafe = function(name, type)
{
	'use strict';
	
	var listenerFunction = function()
	{
		this.onClickGoToActivity(name);
	};
	listenerFunction = listenerFunction.bind(this);
	this.getButton(name).addEventListener('click', listenerFunction, false);
};

GameSimpleMainMenu.prototype.addDefaultEventListenerToButton = function(name, type)
{
	'use strict';
	
	if(!this.namesOfElements.has(name))
		throw new Error(name +' is not an element of the menu');
	
	if(typeof(type) === 'undefined')
		throw new Error('type argument is needed');
	if(!isString(type))
		throw new Error('type argument must be a string');
	if(!GameSimpleMainMenu.DEFAULT_BUTTON_EVENT_LISTENER_TYPES.has(type))
		throw new Error('There is no default event type for '+ type);
	
	this.addDefaultEventListenerToButtonUnsafe(name, type);
};

GameSimpleMainMenu.prototype.addDefaultEventListenersToButton = function(name)
{
	'use strict';
	
	if(!this.namesOfElements.has(name))
		throw new Error(name +' is not an element of the menu');
	
	var types = GameSimpleMainMenu.DEFAULT_BUTTON_EVENT_LISTENER_TYPES.toArray();
	for(var index = 0; index < types.length; ++index)
	{
		this.addDefaultEventListenerToButtonUnsafe(name, types[index]);
	}
};
