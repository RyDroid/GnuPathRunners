/*
 * Copyright (C) 2015, 2017  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/** const */
var GNU_RUNNER_ALIVE_DEFAULT_IMAGE_PATH = 'images/runners/gnu-head_30-years-anniversary.svg';

/** const */
var GNU_RUNNER_DEAD_DEFAULT_IMAGE_PATH  = 'images/runners/gnu-head_ghost.svg';


/**
 * @constructor
 * @param {string|String=} img_path
 */
function GnuRunner(img_path)
{
	"use strict";
	
	if(isString(img_path))
		this.imageAlive = ImagesStatusCache.getImageStatus(img_path);
	else
		this.setDefaultImageAlive();
	this.setDefaultImageDead();
	
	this.isAlive = true;
	
	this.nbGoodItemsCollected = 0;
}

/**
 * @return {ImageStatus}
 */
GnuRunner.prototype.getImageStatus = function()
{
	"use strict";
	return this.isAlive ? this.imageAlive : this.imageDead;
};

/**
 * @return {Image}
 */
GnuRunner.prototype.getImage = function()
{
	"use strict";
	return this.getImageStatus().getImage();
};

GnuRunner.prototype.setDefaultImageAlive = function()
{
	"use strict";
	this.imageAlive = ImagesStatusCache.getImageStatus(GNU_RUNNER_ALIVE_DEFAULT_IMAGE_PATH);
};

GnuRunner.prototype.setDefaultImageDead = function()
{
	"use strict";
	this.imageDead = ImagesStatusCache.getImageStatus(GNU_RUNNER_DEAD_DEFAULT_IMAGE_PATH);
};

/**
 * @return {boolean}
 */
GnuRunner.prototype.isReady = function()
{
	"use strict";
	return this.imageAlive.isReady && this.imageDead.isReady;
};

/**
 * @param {ObjectDrawing2DInformation} drawingInformation
 */
GnuRunner.prototype.setDrawingInformation = function(drawingInformation)
{
	if(!(drawingInformation instanceof ObjectDrawing2DInformation))
		throw new TypeError('drawingInformation must be an instance of ObjectDrawing2DInformation');
	this.drawingInformation = drawingInformation;
};

GnuRunner.prototype.die = function()
{
	"use strict";
	this.isAlive = false;
};

/**
 * @param {RunnerPathItem} anObject
 */
GnuRunner.prototype.hitBy = function(anObject)
{
	"use strict";
	
	if(anObject instanceof RunnerPathItem)
	{
		if(anObject.isGood)
		{
			++this.nbGoodItemsCollected;
			/* TODO */
		}
		else
			this.die();
	}
};

/**
 * @return {number|Number}
 */
GnuRunner.prototype.getScore = function()
{
	"use strict";
	return this.nbGoodItemsCollected;
};
