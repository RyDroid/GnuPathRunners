/*
 * Copyright (C) 2015, 2017  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @constructor
 */
function RunnerPath()
{
	"use strict";
	this.runner = null;
	this.items = [];
	this.distance = 0;
	this.nbBadItemsDodged = 0;
}

RunnerPath.RUNNER_WIDTH_FACTOR = 0.8;
RunnerPath.ITEM_WIDTH_FACTOR = RunnerPath.RUNNER_WIDTH_FACTOR * RunnerPath.RUNNER_WIDTH_FACTOR;


/**
 * @return {number|Number} number of items that are ready
 */
RunnerPath.prototype.getNbItemsReady = function()
{
	"use strict";
	
	var nb = 0;
	for(var index=0; index < this.items.length; ++index)
	{
		var item = this.items[index];
		if(item.isReady())
		{
			++nb;
		}
	}
	return nb;
};

/**
 * @return {number|Number} number of elements that are ready
 */
RunnerPath.prototype.getNbElementsReady = function()
{
	"use strict";
	
	var nb;
	
	if(this.runner instanceof GnuRunner)
	{
		if(this.runner.isReady())
		{
			nb = 1;
		}
		else
		{
			nb = 0;
		}
	}
	else
	{
		nb = 1;
	}
	
	nb += this.getNbItemsReady();
	
	return nb;
};

/**
 * @return {number|Number} number of elements
 */
RunnerPath.prototype.getNbElements = function()
{
	"use strict";
	return 1 + this.items.length;
};

/**
 * @return {number|Number}
 */
RunnerPath.prototype.getPercentageReady = function()
{
	"use strict";
	return this.getNbElementsReady() / this.getNbElements();
};

/**
 * @return {boolean}
 */
RunnerPath.prototype.isReady = function()
{
	"use strict";
	
	if(this.runner != null && !(this.runner instanceof GnuRunner && this.runner.isReady()))
	{
		return false;
	}
	
	for(var index=0; index < this.items.length; ++index)
	{
		var item = this.items[index];
		if(!item.isReady())
		{
			return false;
		}
	}
	return true;
};

/**
 * @return {number|Number}
 */
RunnerPath.prototype.getNormalItemWidth = function()
{
	"use strict";
	return this.drawingInformation.width * RunnerPath.ITEM_WIDTH_FACTOR;
};

RunnerPath.prototype.getNormalItemHeight = RunnerPath.prototype.getNormalItemWidth;

RunnerPath.prototype.resetItemDrawingInformation = function(item)
{
	"use strict";
	
	item.drawingInformation.width  = this.getNormalItemWidth();
	item.drawingInformation.height = this.getNormalItemHeight();
	item.drawingInformation.position.x = this.drawingInformation.position.x + this.drawingInformation.width * (1 - RunnerPath.ITEM_WIDTH_FACTOR) / 2;
};

RunnerPath.prototype.setItemDrawingInformation = function(item)
{
	"use strict";
	
	item.setDrawingInformation(this.drawingInformation.getCopy());
	this.resetItemDrawingInformation(item);
	item.drawingInformation.position.y = - item.drawingInformation.height;
};

RunnerPath.prototype.setRunnerDrawingInformation = function()
{
	"use strict";
	
	if(this.runner instanceof GnuRunner)
	{
		this.runner.setDrawingInformation(this.drawingInformation.getCopy());
		this.runner.drawingInformation.width *= RunnerPath.RUNNER_WIDTH_FACTOR;
		this.runner.drawingInformation.height = this.runner.drawingInformation.width;
		this.runner.drawingInformation.position.x += this.drawingInformation.width * (1 - RunnerPath.RUNNER_WIDTH_FACTOR) / 2;
		this.runner.drawingInformation.position.y = this.drawingInformation.height - this.runner.drawingInformation.width;
	}
};

RunnerPath.prototype.setDrawingInformation = function(drawingInformation)
{
	"use strict";
	
	if(!(drawingInformation instanceof ObjectDrawing2DInformation))
	{
		throw new TypeError('drawingInformation must be an instance of ObjectDrawing2DInformation');
	}
	this.drawingInformation = drawingInformation;
	
	this.setRunnerDrawingInformation();
	
	for(var index=0; index < this.items.length; ++index)
	{
		var item = this.items[index];
		this.resetItemDrawingInformation(item);
	}
};

RunnerPath.prototype.update = function(clock, distancePerMicroseconds)
{
	"use strict";
	
	for(var index=0; index < this.items.length; ++index)
	{
		var item = this.items[index];
		item.update(clock, distancePerMicroseconds);
		
		if(item.drawingInformation.position.y > this.drawingInformation.height)
		{
			if(!item.isGood)
			{
				++this.nbBadItemsDodged;
			}
			
			this.items.splice(index, 1);
		}
		else if(
			this.runner instanceof GnuRunner &&
			this.runner.drawingInformation.isInCollisionWith(item.drawingInformation))
		{
			this.runner.hitBy(item);
			this.items.splice(index, 1);
		}
	}
};

/**
 * @return {number|Number} score
 */
RunnerPath.prototype.getScore = function()
{
	"use strict";
	
	var score = this.nbBadItemsDodged;
	if(this.runner instanceof GnuRunner)
	{
		score += this.runner.getScore();
	}
	return score;
};
