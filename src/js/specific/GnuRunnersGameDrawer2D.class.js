/*
 * Copyright (C) 2015, Nicola Spanti (also known as RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


var GnuRunnersGameDrawer2D = {};

GnuRunnersGameDrawer2D.RUNNER_PATH_RUNNER_WIDTH_FACTOR = 0.8;
GnuRunnersGameDrawer2D.RUNNER_PATH_ITEM_WIDTH_FACTOR = GnuRunnersGameDrawer2D.RUNNER_PATH_RUNNER_WIDTH_FACTOR * GnuRunnersGameDrawer2D.RUNNER_PATH_RUNNER_WIDTH_FACTOR;

/**
 * @param {HTMLCanvasElement}        canvas
 * @param {CanvasRenderingContext2D} context
 */
GnuRunnersGameDrawer2D.drawBackground = function(canvas, context)
{
	"use strict";
	ObjectDrawer2D.drawBackgroundWithStyle(
		canvas, context, "rgb(240, 240, 240)"
	);
};

/**
 * @param {CanvasRenderingContext2D} context
 * @param {RunnerPathItem}           item
 */
GnuRunnersGameDrawer2D.drawRunnerPathItem = function(context, item)
{
	"use strict";
	
	// This verification "fix" problem with undefined/null
	// and not fully loaded images (NS_ERROR_NOT_AVAILABLE)
	if(item instanceof RunnerPathItem && item.isReady())
	{
		ObjectDrawer2D.drawImage(
			context,
			item.getImage(),
			item.drawingInformation
		);
	}
};

/**
 * @param {CanvasRenderingContext2D} context
 * @param {Array.<RunnerPathItem>}   items
 */
GnuRunnersGameDrawer2D.drawRunnerPathItems = function(context, items)
{
	"use strict";
	for(var index = 0; index < items.length; ++index)
	{
		var item = items[index];
		GnuRunnersGameDrawer2D.drawRunnerPathItem(context, item);
	}
};

/**
 * @param {CanvasRenderingContext2D} context
 * @param {GnuRunner}                runner
 */
GnuRunnersGameDrawer2D.drawGnuRunner = function(context, runner)
{
	"use strict";
	
	if(runner instanceof GnuRunner && runner.isReady())
	{
		ObjectDrawer2D.drawImage(
			context,
			runner.getImage(),
			runner.drawingInformation
		);
	}
};

/**
 * @param {CanvasRenderingContext2D} context
 * @param {RunnerPath}               path
 */
GnuRunnersGameDrawer2D.drawRunnerPath = function(context, path)
{
	"use strict";
	
	if(path instanceof RunnerPath)
	{
		GnuRunnersGameDrawer2D.drawRunnerPathItems(context, path.items);
		GnuRunnersGameDrawer2D.drawGnuRunner(context, path.runner);
	}
};

/**
 * @param {CanvasRenderingContext2D} context
 * @param {Array.<RunnerPath>}       paths
 */
GnuRunnersGameDrawer2D.drawRunnerPaths = function(context, paths)
{
	"use strict";
	
	for(var index = 0; index < paths.length; ++index)
	{
		var path = paths[index];
		GnuRunnersGameDrawer2D.drawRunnerPath(context, path);
	}
};

/**
 * @param {CanvasRenderingContext2D} context
 * @param {NearRunnerPathsGroup}     pathsGroup
 */
GnuRunnersGameDrawer2D.drawNearRunnerPathsGroup = function(context, pathsGroup)
{
	"use strict";
	
	GnuRunnersGameDrawer2D.drawRunnerPaths(
		context,
		pathsGroup.paths
	);
	
	if(pathsGroup.drawingInformation.position.x > 0)
	{
		context.beginPath();
		context.moveTo(pathsGroup.drawingInformation.position.x, 0);
		context.lineTo(pathsGroup.drawingInformation.position.x, pathsGroup.drawingInformation.height);
		context.stroke();
	}
};

/**
 * @param {CanvasRenderingContext2D}     context
 * @param {Array.<NearRunnerPathsGroup>} paths
 */
GnuRunnersGameDrawer2D.drawNearRunnerPathsGroups = function(context, paths)
{
	"use strict";
	
	for(var index = 0; index < paths.length; ++index)
	{
		var path = paths[index];
		GnuRunnersGameDrawer2D.drawNearRunnerPathsGroup(
			context, path
		);
	}
};

/**
 * @return {number|Number}
 */
GnuRunnersGameDrawer2D.getTopPanelHeightFromCanvas = function(canvas)
{
	"use strict";
	return parseInt(canvas.height / 10, 10);
};

/**
 * @return {number|Number}
 */
GnuRunnersGameDrawer2D.getTopPanelHeightFromGame = function(game)
{
	"use strict";
	return GnuRunnersGameDrawer2D.getTopPanelHeightFromCanvas(game.canvas);
};

/**
 * @param  {number|Number} topPanelHeight
 * @return {number|Number}
 */
GnuRunnersGameDrawer2D.getTopPanelFontSizeFromHeight = function(topPanelHeight)
{
	"use strict";
	return parseInt(topPanelHeight * 0.8, 10);
};

/**
 * @return {number|Number}
 */
GnuRunnersGameDrawer2D.getTopPanelFontSizeFromGame = function(game)
{
	"use strict";
	var topPanelHeight = GnuRunnersGameDrawer2D.getTopPanelHeightFromGame(game);
	var fontSize = GnuRunnersGameDrawer2D.getTopPanelFontSizeFromHeight(topPanelHeight);
	return fontSize;
};

/**
 * @param {GnuRunnersGame} game
 */
GnuRunnersGameDrawer2D.drawTopPanelBackground = function(game)
{
	"use strict";
	
	if(ObjectDrawer2D.current_quality_mode >= ObjectDrawer2D.HIGH_QUALITY_MODE)
	{
		game.context.fillStyle = "rgba(0, 0, 0, 0.5)";
	}
	else
	{
		// A transparent panel is better, but not needed
		game.context.fillStyle = "rgb(150, 150, 150)";
	}
	var topPanelHeight = GnuRunnersGameDrawer2D.getTopPanelHeightFromGame(game);
	game.context.fillRect(0, 0, game.canvas.width, topPanelHeight);
};

/**
 * @param {GnuRunnersGame} game
 */
GnuRunnersGameDrawer2D.drawTopPanelScore = function(game)
{
	"use strict";
	
	var fontSize = GnuRunnersGameDrawer2D.getTopPanelFontSizeFromGame(game);
	game.context.fillStyle = "rgb(240, 240, 240)";
	game.context.font = fontSize +'px "Droid Sans"';
	game.context.textAlign = 'right';
	game.context.fillText(
		new String(game.getScoreValue()),
		game.canvas.width * 0.96, fontSize
	);
};

/**
 * @param {GnuRunnersGame} game
 */
GnuRunnersGameDrawer2D.drawTopPanel = function(game)
{
	"use strict";
	
	if(!game.isOver())
	{
		GnuRunnersGameDrawer2D.drawTopPanelBackground(game);
		GnuRunnersGameDrawer2D.drawTopPanelScore     (game);
	}
};

/**
 * @param {GnuRunnersGame} game
 */
GnuRunnersGameDrawer2D.drawGame = function(game)
{
	"use strict";
	
	GnuRunnersGameDrawer2D.drawBackground(game.canvas, game.context);
	GnuRunnersGameDrawer2D.drawNearRunnerPathsGroups(game.context, game.paths);
	GnuRunnersGameDrawer2D.drawTopPanel(game);
};
